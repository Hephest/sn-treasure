# Treasure Hunt Solver
by Vladyslav Asaievych ([@Hephest](http://gitlab.com/Hephest))

Script allows the user to solve 'Treasure Hunt' test task from StarNavi

## Functional

### How to use
1. Enter matrix elements into `input.txt`
2. Run script by `python functional/solver.py`
3. Get your path to treasure!

### Tests

Tests writed and executed using `pytest` package:

```shell script
D:\Projects\starnavi\treasure\functional>py.test -svv
===================================================== test session starts =====================================================
platform win32 -- Python 3.7.3, pytest-5.2.0, py-1.8.0, pluggy-0.13.0 -- c:\python\python37-32\python.exe
cachedir: .pytest_cache
rootdir: D:\Projects\starnavi\treasure\functional
plugins: cov-2.7.1
collected 7 items                                                                                                              

test_solver.py::TestMatrixClass::test_matrix_size PASSED
test_solver.py::TestMatrixClass::test_matrix_all_elements_is_int PASSED
test_solver.py::TestMatrixClass::test_matrix_random_element PASSED
test_solver.py::TestSolverClass::test_solver_main_method_works [11, 55, 15, 21, 44, 32, 13, 25, 43]
PASSED
test_solver.py::TestSolverClass::test_solver_example_1 PASSED
test_solver.py::TestSolverClass::test_solver_example_2 PASSED
test_solver.py::TestSolverClass::test_solver_example_treasure_at_start PASSED

====================================================== 7 passed in 0.09s ======================================================
```

Example of coverage, using `pytest-cov` package:

```shell script
D:\Projects\starnavi\treasure\functional>py.test --cov=functional --cov-report=term-missing
===================================================== test session starts =====================================================
platform win32 -- Python 3.7.3, pytest-5.2.0, py-1.8.0, pluggy-0.13.0
rootdir: D:\Projects\starnavi\treasure\functional
plugins: cov-2.7.1
collected 7 items                                                                                                              

test_solver.py .......                                                                                                   [100%]

----------- coverage: platform win32, python 3.7.3-final-0 -----------
Name             Stmts   Miss  Cover   Missing
----------------------------------------------
__init__.py          0      0   100%
solver.py           24      1    96%   76
test_solver.py      38      0   100%
----------------------------------------------
TOTAL               62      1    98%


====================================================== 7 passed in 0.14s ======================================================
```

## Object-oriented

### How to use
1. Enter matrix elements into `input.txt`
2. Run script by `python object_oriented/solver.py`
3. Get your path to treasure!

### Tests

Tests writed and executed using `pytest` package:

```shell script
D:\Projects\starnavi\treasure\object_oriented>pytest -svv
===================================================== test session starts =====================================================
platform win32 -- Python 3.7.3, pytest-5.2.0, py-1.8.0, pluggy-0.13.0 -- d:\projects\starnavi\treasure\venv\scripts\python.exe
cachedir: .pytest_cache
rootdir: D:\Projects\starnavi\treasure\object_oriented
plugins: cov-2.7.1
collected 12 items                                                                                                             

test_solver.py::TestMatrixClass::test_matrix_class_instance PASSED
test_solver.py::TestMatrixClass::test_matrix_class_error_no_filename PASSED
test_solver.py::TestMatrixClass::test_matrix_size PASSED
test_solver.py::TestMatrixClass::test_matrix_all_elements_is_int PASSED
test_solver.py::TestMatrixClass::test_matrix_random_element PASSED
test_solver.py::TestMatrixClass::test_matrix_print [[55, 14, 25, 52, 21], [44, 31, 11, 53, 43], [24, 13, 45, 12, 34], [42, 22, 4
3, 32, 41], [51, 23, 33, 54, 15]]
[[55, 14, 25, 52, 21], [44, 31, 11, 53, 43], [24, 13, 45, 12, 34], [42, 22, 43, 32, 41], [51, 23, 33, 54, 15]]
PASSED
test_solver.py::TestMatrixClass::test_matrix_print_path [11]
[11]
PASSED
test_solver.py::TestMatrixClass::test_solver_main_method_works [[34, 21, 32, 41, 25], [14, 42, 43, 14, 31], [54, 45, 52, 42, 23]
, [33, 15, 51, 31, 35], [21, 52, 33, 13, 32]]
[11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
PASSED
test_solver.py::TestMatrixClass::test_solver_main_methow_returns_path [[34, 21, 32, 41, 25], [14, 42, 43, 14, 31], [54, 45, 52,
42, 23], [33, 15, 51, 31, 35], [21, 52, 33, 13, 32]]
[11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
[[34, 21, 32, 41, 25], [14, 42, 43, 14, 31], [54, 45, 52, 42, 23], [33, 15, 51, 31, 35], [21, 52, 33, 13, 32]]
[11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
None
[11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
PASSED
test_solver.py::TestMatrixClass::test_solver_example_1 [11, 55, 15, 21, 44, 32, 13, 25, 43]
None
[11, 55, 15, 21, 44, 32, 13, 25, 43]
PASSED
test_solver.py::TestMatrixClass::test_solver_example_2 [11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33,
52]
None
[11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
PASSED
test_solver.py::TestMatrixClass::test_solver_example_treasure_at_start [11]
None
[11]
PASSED

===================================================== 12 passed in 0.11s ======================================================
```

Example of coverage, using `pytest-cov` package:

```shell script
D:\Projects\starnavi\treasure\object_oriented>pytest --cov=object_oriented --cov-report=term-missing
===================================================== test session starts =====================================================
platform win32 -- Python 3.7.3, pytest-5.2.0, py-1.8.0, pluggy-0.13.0
rootdir: D:\Projects\starnavi\treasure\object_oriented
plugins: cov-2.7.1
collected 12 items                                                                                                             

test_solver.py ............                                                                                              [100%]

----------- coverage: platform win32, python 3.7.3-final-0 -----------
Name             Stmts   Miss  Cover   Missing
----------------------------------------------
__init__.py          0      0   100%
solver.py           23      1    96%   66
test_solver.py      58      0   100%
----------------------------------------------
TOTAL               81      1    99%


===================================================== 12 passed in 0.17s ======================================================
```