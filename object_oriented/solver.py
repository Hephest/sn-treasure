"""Treasure Hunt Solver (Object-oriented)

by Vladyslav Asaievych

This script allows the user to solve 'Treasure Hunt' test task:

    You need to write a program to explore the above table for a treasure.

    The values in the table are clues. Each cell contains a number between 11 and 55,
    where the ten’s digit represents the row number and the unit’s digit represents
    the column number of the cell containing the next clue. Starting with the upper
    left corner (at 1,1), use the clues to guide your search through the table - (the
    first three clues are 11, 55, 15). The treasure is a cell whose value is the same
    as its coordinates. Your program must first read in the treasure map data into
    a 5 by 5 array.

This tool accepts regular text files (.txt) with 5x5 matrix, separated by spaces and
contains only numbers.

This script's tests requires that `pytest` be installed within the Python
environment you are running tests.
"""


class Matrix:
    """
    The Matrix class, which represents treasure map matrix.
    """
    def __init__(self, filename):
        """Initialises the Matrix class instance by given text file

        :param filename: path to text file with matrix
        :type filename: str
        """
        self.filename = filename
        self.matrix = []
        self.path = [11]

        with open(self.filename, 'r') as f:
            self.matrix = [[int(num) for num in line.split(' ')] for line in f]

    def show_matrix(self):
        """Prints matrix to console."""
        print(self.matrix)

    def show_path(self):
        """Prints path to console."""
        print(self.path)

    def solver(self):
        """Solves the matrix using recursive approach."""
        x, y = self.path[-1] // 10 % 10 - 1, self.path[-1] % 10 - 1
        if self.matrix[x][y] != self.path[-1]:
            self.path.append(self.matrix[x][y])
            self.solver()


def main():
    matrix = Matrix('input2.txt')
    matrix.show_matrix()
    matrix.solver()
    matrix.show_path()


if __name__ == '__main__':
    main()
