import os
import random as rnd

import pytest

from object_oriented.solver import Matrix, main


class TestMatrixClass:

    def test_matrix_class_instance(self):
        m = Matrix('input.txt')
        assert isinstance(m, Matrix)

    def test_matrix_class_error_no_filename(self):
        with pytest.raises(TypeError):
            m = Matrix()

    def test_matrix_size(self):
        m = Matrix('input.txt')
        assert len(m.matrix) == 5

    def test_matrix_all_elements_is_int(self):
        m = Matrix('input.txt')
        for row in m.matrix:
            for elem in row:
                assert type(elem) is int

    def test_matrix_random_element(self):
        m = Matrix('input.txt')
        elem = m.matrix[rnd.randrange(5)][rnd.randrange(5)]
        assert type(elem) is int
        assert elem in range(11, 56)

    def test_matrix_print(self):
        m = Matrix('input.txt')
        assert m.show_matrix() == print(m.matrix)

    def test_matrix_print_path(self):
        m = Matrix('input.txt')
        assert m.show_path() == print(m.path)

    def test_solver_main_method_works(self):
        assert main() is None

    def test_solver_main_methow_returns_path(self):
        path_correct = [11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
        main()
        assert print(main()) == print(path_correct)


    def test_solver_example_1(self):
        m = Matrix('input.txt')
        m.solver()
        path_solved = m.show_path()
        path_correct = [11, 55, 15, 21, 44, 32, 13, 25, 43]
        assert print(path_solved) == print(path_correct)

    def test_solver_example_2(self):
        m = Matrix('input2.txt')
        m.solver()
        path_solved = m.show_path()
        path_correct = [11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
        assert print(path_solved) == print(path_correct)

    def test_solver_example_treasure_at_start(self):
        path_correct = [11]
        with open('test.txt', 'w') as f:
            for line in range(5):
                f.write('11 11 11 11 11\n')
        m = Matrix('test.txt')
        m.solver()
        path_solved = m.show_path()
        os.remove('test.txt')
        assert print(path_solved) == print(path_correct)
