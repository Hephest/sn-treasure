import os
import random as rnd

from functional.solver import read_matrix, solve_matrix, main


class TestMatrixClass:

    def test_matrix_size(self):
        assert len(read_matrix('input.txt')) == 5

    def test_matrix_all_elements_is_int(self):
        matrix = read_matrix('input.txt')
        for row in matrix:
            for elem in row:
                assert type(elem) is int

    def test_matrix_random_element(self):
        matrix = read_matrix('input.txt')
        elem = matrix[rnd.randrange(5)][rnd.randrange(5)]
        assert type(elem) is int
        assert elem in range(11, 56)


class TestSolverClass:

    def test_solver_main_method_works(self):
        assert main() is None

    def test_solver_example_1(self):
        matrix = read_matrix('input.txt')
        path_solved = solve_matrix(matrix)
        path_correct = [11, 55, 15, 21, 44, 32, 13, 25, 43]
        assert path_solved == path_correct

    def test_solver_example_2(self):
        matrix = read_matrix('input2.txt')
        path_solved = solve_matrix(matrix)
        path_correct = [11, 34, 42, 15, 25, 31, 54, 13, 32, 45, 35, 23, 43, 51, 21, 14, 41, 33, 52]
        assert path_solved == path_correct

    def test_solver_example_treasure_at_start(self):
        path_correct = [11]
        with open('test.txt', 'w') as f:
            for line in range(5):
                f.write('11 11 11 11 11\n')
        matrix = read_matrix('test.txt')
        path_solved = solve_matrix(matrix)
        os.remove('test.txt')
        assert path_solved == path_correct
