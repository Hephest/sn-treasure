"""Treasure Hunt Solver (Functional)

by Vladyslav Asaievych

This script allows the user to solve 'Treasure Hunt' test task:

    You need to write a program to explore the above table for a treasure.

    The values in the table are clues. Each cell contains a number between 11 and 55,
    where the ten’s digit represents the row number and the unit’s digit represents
    the column number of the cell containing the next clue. Starting with the upper
    left corner (at 1,1), use the clues to guide your search through the table - (the
    first three clues are 11, 55, 15). The treasure is a cell whose value is the same
    as its coordinates. Your program must first read in the treasure map data into
    a 5 by 5 array.

This tool accepts regular text files (.txt) with 5x5 matrix, separated by spaces and
contains only numbers.

This script's tests requires that `pytest` be installed within the Python
environment you are running tests.

This file can also be imported as a module and contains the following functions:

    * read_matrix - returns matrix from text file as list of lists
    * solve_matrix - returns path to treasure as list of elements
    * main - the main function of the script
"""


def read_matrix(filename):
    """Retrieves the matrix from text file.

    :param filename: path to text file with matrix
    :type filename: str
    :return: list of lists
    :rtype: list
    """
    with open(filename, 'r') as f:
        matrix = [[int(num) for num in line.split(' ')] for line in f]
    return matrix


def solve_matrix(matrix):
    """Solves the matrix and returns path to treasure.

    :param matrix: 2D matrix
    :type matrix: list
    :return: list
    :rtype: list
    """
    path = []
    founded = False
    row, col = 4, 4

    path.append(11)
    while not founded:
        x, y = path[-1] // 10 % 10 - 1, path[-1] % 10 - 1
        if matrix[x][y] != matrix[row][col]:
            row, col = matrix[x-1][y-1] // 10 % 10 - 1, matrix[x-1][y-1] % 10 - 1
            if path[-1] != matrix[x][y]:
                path.append(matrix[x][y])
            else:
                founded = True
        else:
            founded = True

    return path


def main():
    matrix = read_matrix('input.txt')
    path = solve_matrix(matrix)
    print(path)


if __name__ == '__main__':
    main()
